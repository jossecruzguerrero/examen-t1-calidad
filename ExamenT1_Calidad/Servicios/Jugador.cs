﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenT1_Calidad.Servivios
{
    public class Jugador
    {
        public int Id { get; set; }
        public String Nombre { get; set; }
        public List<Puntaje> Punto { get; set; }
        public int Puntaje { get; set; }
    }

}
