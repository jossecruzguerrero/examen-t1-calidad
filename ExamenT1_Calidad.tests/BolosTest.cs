﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Text;
using ExamenT1_Calidad.Servivios;

namespace ExamenT1_Calidad.tests
{
    [TestFixture]
    public class BolosTest
    {
        [Test]
        //Con 0 jugadores
        public void Caso01()
        {
            var bolos = new Bolos();
            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(0, resultado);
        }

        [Test]
        //Con un jugador
        public void Caso02()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Juan" });
            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(1, resultado);
        }
        [Test]
        //Con 2 jugadores
        public void Caso03()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Juan" });
            bolos.RegistrarJugador(new Jugador { Id = 2, Nombre = "Ana" });
            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(2, resultado);
        }

        [Test]
        //Con 3 jugadores
        public void Caso04()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Juan" });
            bolos.RegistrarJugador(new Jugador { Id = 2, Nombre = "Ana" });
            bolos.RegistrarJugador(new Jugador { Id = 3, Nombre = "Pedro" });
            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(3, resultado);
        }

        [Test]
        //Lanzamientos de 0 y 0 Puntos
        public void Caso05()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana",Puntaje = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 0, lanzar2 = 0 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(0, resultado);
        }
    
        [Test]
        //Lanzamientos de 3 y 0 Puntos
        public void Caso06()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana"});
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 5  });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(8, resultado);
        }

        [Test]
        //Lanzamientos de  6 y 2 Puntos
        public void Caso07()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana", Puntaje = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(8, resultado);
        }
        [Test]
        //Lanzamientos de 5 y 5 Puntos
        public void Caso08()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana", Puntaje = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 5, lanzar2 = 5 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(10, resultado);
        }
        [Test]
        //Lanzamientos de 0 y 10 Puntos
        public void Caso09()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana", Puntaje = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 0, lanzar2 = 10 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(10, resultado);
        }
        [Test]
        //Lanzamientos de 10 Puntos
        public void Caso10()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana", Puntaje = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(10, resultado);
        }
        [Test]
        //Puntaje NoSpare NoStrike
        public void Caso11()
        {
            var bolos = new Bolos();

            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana", Puntaje = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 5, lanzar2 = 3 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 0, lanzar2 = 1 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 3 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(33, resultado);
        }
        [Test]
        //Puntaje NoStrike SiSpare
        public void Caso12()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana", Puntaje = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 0, lanzar2 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 6 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 3 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 3, lanzar2 = 6 });

            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(54, resultado);
        }
        [Test]
        //Puntaje NoSpare SiStrike
        public void Caso13()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana",Puntaje = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 4 });
            
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(62, resultado);
        }
        [Test]
        //Puntaje NoSpare SiStrikeFinal
        public void Caso14()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana", Puntaje = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(80, resultado);
            
        }
        [Test]
        //Puntaje SiSpare NoStrikeFinal10
        public void Caso15()
        {
            var bolos = new Bolos();
            bolos.RegistrarJugador(new Jugador { Id = 1, Nombre = "Ana", Puntaje = 0 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 0, lanzar2 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 6 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 2, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 1 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(55, resultado);
        }
        
        [Test]
        //PuntajeNoSpareSiStrikeFinal10Y6y4
        public void Caso16()
        {
            var bolos = new Bolos();
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 4 });/*
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });*/
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(22, resultado);
        }
        /*
        [Test]
        //PuntajeNoSpareSiStrikeFinal10Y10
        public void Casox()
        {
            var bolos = new Bolos();
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 2 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(82, resultado);
        }
        [Test]
        //PuntajeSiSpareSiStrikeAlFinal
        public void Casoy()
        {
            var bolos = new Bolos();
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 5, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 0, lanzar2 = 10 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 4, lanzar2 = 6 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 2, lanzar2 = 5 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 6, lanzar2 = 4 });
            bolos.RegistrarPuntaje(new Puntaje { IdJugador = 1, lanzar1 = 10 });
            var resultado = bolos.GetPuntajeSprit();
            Assert.AreEqual(60, resultado);
        }*/
    }
}